import React from "react";
import "./List.css";

export default function List(props) {
    const sports = [
        "basketball",
        "baseball",
        "football",
        "soccer",
        "lacrosse",
        "volleyball",
        "rugby",
        "badminton",
        "tennis",
        "golf",
        "cycling",
        "wrestling",
        "boxing",
        "fencing",
        "bowling",
        "surfing",
        "hockey",
        "table tennis",
        "curling",
        "snowboarding",
        "skiing",
        "wakeboarding",
        "cricket",
        "rowing",
        "racquetball",
        "handball",
        "field hockey"
    ];

    return (
        <div className="list-container">
            {sports
                .filter((sport) => sport.includes(props.searchTerm.toLowerCase()))
                .map((sport, i) => (
                    <div key={i} className="sport">
                        {sport}
                    </div>
                ))}
        </div>
    );
}

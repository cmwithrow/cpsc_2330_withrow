import React, { useState } from "react";
import List from "./components/List/List";
import Search from "./components/Search/Search";
import "./App.css";

function App() {
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <div className="App">
            <Search onChange={(e) => setSearchTerm(e.target.value)} />
            <List searchTerm={searchTerm} />
        </div>
    );
}

export default App;

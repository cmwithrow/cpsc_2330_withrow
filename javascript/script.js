"use strict";

//DON'T USE VAR
// console.log(myVariable);
// var myVariable = "Brian";

//let
let myInt = 5;
let myString = "string";
let myObject = {};
let myArray = [1, "string", [], {}];

myInt = "five";

console.log(typeof myInt);
console.log(typeof myObject);

// ==
// Compares value
console.log("5" == 5);

// ===
// Compares value and type
console.log("5" === 5);

//const
const myVal = 5;
// Error:
// myVal = 6;

//document - hardcoded var refer to entire html
// console.log(document);

// takes a css selector argument and returns the FIRST element found
const button2 = document.querySelector("#button2");

button2.onClick = () => logConsoleMessage();

function logConsoleMessage() {
    console.log("This is an output to console");
}

function displayAlert(alertText = "This was caused by the button click event") {
    alert(alertText);
}

const examples = document.getElementsByClassName("example");

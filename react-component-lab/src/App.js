import React, { useState } from "react";
import "./App.css";
import MovieList from "./components/MovieList/MovieList";
import MadLib from "./components/MadLib/MadLib";

function App() {
    const [movieList, setMovieList] = useState(false);
    const [madLib, setMadLib] = useState(false);

    /**
     * toogle movie rendering on checkbox
     */
    const handleMovieCheck = () => {
        movieList ? setMovieList(false) : setMovieList(true);
    };
    /**
     * toggle madlib render on checkbox
     */
    const handleMadLibCheck = () => {
        madLib ? setMadLib(false) : setMadLib(true);
    };

    return (
        <div className="App">
            <h1>React Component Lab</h1>
            <input className="checkbox" type="checkbox" name="movie" onChange={handleMovieCheck} />
            <label className="check-label" htmlFor="movies">
                Movies
            </label>
            <input className="checkbox" type="checkbox" name="madlib" onChange={handleMadLibCheck} />
            <label className="check-label" htmlFor="madlib">
                Mad Lib
            </label>
            {movieList && <MovieList />}
            {madLib && <MadLib />}
        </div>
    );
}

export default App;

import "./MovieList.css";
export default function MovieList() {
    // movies data
    const movies = [
        { id: 1, title: "Star Wars", releaseDate: 1977, boxOffice: "775.8 million" },
        { id: 2, title: "Ferris Bueller's Day Off", releaseDate: 1986, boxOffice: "70.1 million" },
        { id: 3, title: "Forrest Gump", releaseDate: 1994, boxOffice: "683.1 million" },
        { id: 4, title: "Avengers: Endgame", releaseDate: 2019, boxOffice: "2.798 billion" },
        { id: 5, title: "Cats", releaseDate: 2019, boxOffice: "75.5 million" }
    ];

    return (
        <div className="card">
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Release Date</th>
                        <th>Box Office</th>
                    </tr>
                </thead>
                <tbody>
                    {movies.map((movie) => (
                        <tr key={movie.id}>
                            <td>{movie.title}</td>
                            <td>{movie.releaseDate}</td>
                            <td>{movie.boxOffice}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

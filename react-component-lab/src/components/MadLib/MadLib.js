import React, { useState } from "react";
import "./MadLib.css";

export default function MadLib() {
    // Madlib
    const [person, setPerson] = useState("Person");
    const [thing, setThing] = useState("Thing");
    const [adjective, setAdjective] = useState("Nice");

    return (
        <div className="container">
            <div className="form">
                <div className="field">
                    <label htmlFor="person">Person</label>
                    <input type="text" name="person" onChange={(e) => setPerson(e.target.value)} />
                </div>
                <div className="field">
                    <label htmlFor="thing">Thing</label>
                    <input type="text" name="thing" onChange={(e) => setThing(e.target.value)} />
                </div>
                <div className="field">
                    <label htmlFor="adj">Adjective</label>
                    <input type="text" name="adj" onChange={(e) => setAdjective(e.target.value)} />
                </div>
            </div>
            <div className="story">
                <h2>Once Upon a Time...</h2>
                <p>
                    ...in a land far away, there lived <span className="emphasize">{person}</span>.{" "}
                    <span className="emphasize">{person}</span> was a lowly peasant but knew that if they could get to
                    the castle and bring back the magic <span className="emphasize">{thing}</span>, they would be
                    cherished by all the poor folk in their small town. There was only one small problem, the magic{" "}
                    <span className="emphasize">{thing}</span> was a magic,{" "}
                    <span className="emphasize">{adjective}</span> <span className="emphasize">{thing}</span>. That
                    would make things more difficult. Nonetheless, he set out on his journey... to be continued...
                </p>
            </div>
        </div>
    );
}

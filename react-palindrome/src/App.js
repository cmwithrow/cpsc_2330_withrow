import React, { useState } from "react";
import "./App.css";

function App() {
    const [result, setResult] = useState("");
    const [value, setValue] = useState("");

    const checkIsPalindrome = () => {
        const input = value.replace(" ", "").toLowerCase();
        if (input.length > 1) {
            const reverseStr = input.split("").reverse().join("");
            const display = input === reverseStr ? "Yep, it's a Palindrome" : "Nope, not a Palindrome";
            setResult(display);
        }
    };

    const handleChange = (e) => {
        setValue(e.target.value);
        setResult("");
    };

    return (
        <div className={"app"}>
            <h1>Check if word is a palindrome...</h1>
            <input value={value} onChange={handleChange} name="palindrome" placeholder="Possible Palindrome" />
            <button onClick={checkIsPalindrome}>&gt;</button>
            <div className="result">{result}</div>
        </div>
    );
}

export default App;

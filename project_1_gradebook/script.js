"use strict";

const navHome = document.getElementById("nav-home");
const navBook = document.getElementById("nav-book");
const home = document.getElementById("home");
const gradebook = document.getElementById("gradebook");
const tableBody = document.getElementById("tableBody");
const nameInput = document.getElementById("assignmentName");
const awardedPtsInput = document.getElementById("pointsAwarded");
const totalPtsInput = document.getElementById("totalPoints");
const addButton = document.getElementById("addButton");

const modal = document.getElementsByClassName("modal")[0];
const cancelModal = document.getElementById("modal-cancel");
const deleteModal = document.getElementById("modal-delete");

const overallPct = document.getElementById("overallPct");
const overallLetter = document.getElementById("overallLetter");

navHome.addEventListener("click", handleGoHome);
navBook.addEventListener("click", handleGoGradebook);
nameInput.addEventListener("input", validateInputs);
awardedPtsInput.addEventListener("input", validateInputs);
totalPtsInput.addEventListener("input", validateInputs);
addButton.addEventListener("click", addRecord);
cancelModal.addEventListener("click", closeModal);

let assignments = [];
let idVal = 0;
let idToDelete;

/**
 * Change to home page
 */
function handleGoHome() {
    home.className = "";
    gradebook.className = "hide-page";
    navHome.className = "nav-button nav-active";
    navBook.className = "nav-button";
}

/**
 * Change to Gradebook
 */
function handleGoGradebook() {
    home.className = "hide-page";
    gradebook.className = "";
    navHome.className = "nav-button";
    navBook.className = "nav-button nav-active";
}

/**
 * Validate correct grade entries
 */
function validateInputs() {
    if (nameInput.value && awardedPtsInput.value && totalPtsInput.value) {
        try {
            const x = parseFloat(awardedPtsInput.value);
            const y = parseFloat(totalPtsInput.value);

            console.log(x, y);
            addButton.setAttribute("disabled", "");
            console.log(x, y);
        } catch {
            console.log("Not Int");
            addButton.setAttribute("disabled", "");
        }
    } else {
        addButton.setAttribute("disabled", "");
    }
}

/**
 * Add entry to assignments array
 */
function addRecord() {
    assignments.unshift({
        name: nameInput.value,
        awardedPts: awardedPtsInput.value,
        totalPts: totalPtsInput.value,
        id: idVal
    });
    idVal++;
    renderTableBody();
    setStorage(assignments);
    nameInput.value = "";
    awardedPtsInput.value = "";
    totalPtsInput.value = "";
    validateInputs();
}

/**
 * Map assignments array and re-render gradebook table
 */
function renderTableBody() {
    tableBody.innerHTML = "";

    let allPts = 0;
    let allAwardedPts = 0;

    assignments.map((assignment) => {
        const tableRow = tableBody.insertRow(-1);
        const nameCell = tableRow.insertCell(0);
        const awardedPtsCell = tableRow.insertCell(1);
        const totalPtsCell = tableRow.insertCell(2);
        const actionCell = tableRow.insertCell(3);

        nameCell.innerText = assignment.name;
        awardedPtsCell.innerText = assignment.awardedPts;
        totalPtsCell.innerText = assignment.totalPts;
        actionCell.innerHTML = `<button onclick="openModal(${assignment.id})">Delete</button>`;

        allPts += parseFloat(assignment.totalPts);
        allAwardedPts += parseFloat(assignment.awardedPts);
    });

    updateGradeTotals(allAwardedPts, allPts);
}

/**
 * Calculate total grade and letter grade
 * @param {*} awarded 
 * @param {*} all 
 */
function updateGradeTotals(awarded, all) {
    awarded = awarded;
    all = all;

    if (assignments.length > 0) {
        const pct = (100 * (awarded / all)).toFixed(1);
        let letter;

        switch (true) {
            case pct >= 93:
                letter = "A";
                break;
            case pct >= 90:
                letter = "A-";
                break;
            case pct >= 87:
                letter = "B+";
                break;
            case pct >= 83:
                letter = "B";
                break;
            case pct >= 80:
                letter = "B-";
                break;
            case pct >= 77:
                letter = "C+";
                break;
            case pct >= 73:
                letter = "C";
                break;
            case pct >= 70:
                letter = "C-";
                break;
            case pct >= 67:
                letter = "D+";
                break;
            case pct >= 63:
                letter = "D";
                break;
            case pct >= 60:
                letter = "D-";
                break;
            case pct < 60:
                letter = "F";
                break;
        }

        overallPct.innerText = `${pct.toString()}%`;
        overallLetter.innerText = letter.toString();
    } else {
        overallPct.innerText = "N/A";
        overallLetter.innerText = "N/A";
    }
}

/**
 * Close confirmation modal
 */
function closeModal() {
    modal.style.display = "none";
}

/**
 * Open delete confirmation modal w/ assignment id
 * @param {*} id 
 */
function openModal(id) {
    modal.style.display = "block";
    deleteModal.setAttribute("onClick", `javascript: deleteAssignment(${id});`);
}

/**
 * Attempt to load and set assignmets from local storage
 */
function getDataFromStorage() {
    let storageData = window.localStorage.getItem("grades");
    if (storageData) {
        try {
            assignments = JSON.parse(storageData);

            idVal =
                Math.max.apply(
                    Math,
                    assignments.map((myAssignment) => myAssignment.id)
                ) + 1;

            handleGoGradebook();

            renderTableBody();
        } catch (e) {
            console.log(e);
            assignments = [];

            handleGoHome();
        }
    }
}

/**
 * Remove assignments from assignments array
 * @param {*} id 
 */
function deleteAssignment(id) {
    console.log("delete");
    const index = assignments
        .map((x) => {
            return x.id;
        })
        .indexOf(id);
    assignments.splice(index, 1);
    setStorage(assignments);
    renderTableBody();
    closeModal();
}

/**
 * Set local storage to current assignmetns array
 * @param {array of assignments} assignments
 */
function setStorage(assignments) {
    try {
        window.localStorage.setItem("grades", JSON.stringify(assignments));
    } catch (e) {
        console.log(e);
    }
}

// retrieve assignments from local storage on page load
getDataFromStorage();

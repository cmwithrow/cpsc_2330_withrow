import "./App.css";
import biography from "./images/bio.png";
import coin from "./images/coin.png";
import grade from "./images/grade.png";
import pokemon from "./images/pokemon.png";
import { FaExternalLinkAlt } from "react-icons/fa";
import { IoLogoBitbucket } from "react-icons/io";

function App() {
    const projects = [
        {
            id: 0,
            name: "biography page",
            description:
                "A profile all about me! Simply html and css and nothing more. Get to know me and my interests.",
            thumbnail: biography,
            netlify: "https://camden-biography.netlify.app",
            bitbucket: "https://bitbucket.org/cmwithrow/cpsc_2330_withrow/src/master/biography_page/"
        },
        {
            id: 1,
            name: "coinflip app",
            description:
                "You ever feel that internal dread about making a decision? Boy do I have the revolutionary solution for you...",
            thumbnail: coin,
            netlify: "https://camden-coinflip.netlify.app",
            bitbucket: "https://bitbucket.org/cmwithrow/cpsc_2330_withrow/src/master/coinflip_app/"
        },
        {
            id: 2,
            name: "gradebook app",
            description: "Teach your students a lesson by given them a grade they remember.",
            thumbnail: grade,
            netlify: "https://cwithrow-cpsc2330.netlify.app",
            bitbucket: "https://bitbucket.org/cmwithrow/cpsc_2330_withrow/src/master/project_1_gradebook/"
        },
        {
            id: 3,
            name: "pokemon app",
            description: "Who's that pokemon?!?! Find out now... here... seriously try it out. Start a collection.",
            thumbnail: pokemon,
            netlify: "https://project2-cwithrow.netlify.app",
            bitbucket: "https://bitbucket.org/cmwithrow/cpsc_2330_withrow/src/master/project2/"
        }
    ];

    return (
        <div className="app">
            <div className="hero">
                <div className="hero-content">
                    <h1>
                        <span>Hello World,</span> I'm Camden
                    </h1>
                    <h6 className="profile">
                        I am a student at Anderson University in Anderson, IN. I am studying Mathematics Finance,
                        Computer Science, and Spanish. I am currently working at Genesys as a Software Engineer Intern
                        and have a side gig at the campus coffee shop.
                    </h6>
                </div>
            </div>
            <div className="projects-container">
                {projects.map((project) => (
                    <div class="card" key={project.id}>
                        <div className="thumbnail">
                            <img src={project.thumbnail} alt={`thumbnail for ${project.name}`} />
                        </div>
                        <div className="card-content">
                            <h3>{project.name}</h3>
                            <p>{project.description}</p>
                        </div>
                        <div className="card-actions">
                            <a href={project.bitbucket} target="_blank">
                                <IoLogoBitbucket size={20} />
                            </a>
                            <a href={project.netlify} target="_blank">
                                <FaExternalLinkAlt size={20} />
                            </a>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default App;

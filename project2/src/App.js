import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import List from "./components/List";
import PokemonAdd from "./components/PokemonAdd";
import PokemonView from "./components/PokemonView";

function App() {
    const [currentComponent, setCurrentComponent] = useState("list");
    const [currentIds, setCurrentIds] = useState([]);
    const [idIndex, setIdIndex] = useState();

    return (
        <React.Fragment>
            <Container maxWidth="md">
                <img
                    src="https://pngimg.com/uploads/pokemon_logo/pokemon_logo_PNG3.png"
                    alt="Pokemon Logo"
                    style={{ maxWidth: "100%" }}
                />
            </Container>
            <Container maxWidth="md" style={{ marginTop: "64px" }}>
                {currentComponent === "view" && (
                    <PokemonView ids={currentIds} index={idIndex} setComponent={setCurrentComponent} />
                )}
                {currentComponent === "add" && <PokemonAdd setComponent={setCurrentComponent} />}
                {currentComponent === "list" && (
                    <List setComponent={setCurrentComponent} setIds={setCurrentIds} setIndex={setIdIndex} />
                )}
            </Container>
        </React.Fragment>
    );
}

export default App;

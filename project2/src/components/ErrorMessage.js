import React, { useState } from "react";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";

export default function ErrorMessage(props) {
    const [openSnack, setOpenSnack] = useState(true);

    /**
     * CloseSnack bar only if not clickaway
     * @param {*} e 
     * @param {*} reason 
     * @returns 
     */
    const handleCloseSnack = (e, reason) => {
        if (reason === "clickaway") {
            return;
        }

        setOpenSnack(false);
        props.reset();
    };

    return (
        <React.Fragment>
            <Snackbar open={openSnack} autoHideDuration={6000} onClose={handleCloseSnack}>
                <Alert elevation={6} variant="filled" onClose={handleCloseSnack} severity="error">
                    {props.message}
                </Alert>
            </Snackbar>
        </React.Fragment>
    );
}

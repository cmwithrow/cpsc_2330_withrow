import React, { useState, useEffect } from "react";
import axios from "axios";
import ErrorMessage from "./ErrorMessage";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CasinoIcon from "@material-ui/icons/Casino";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    card: {
        marginLeft: "auto",
        marginRight: "auto",
        maxWidth: "400px"
    },
    input: {
        margin: "10px",
        display: "block",
        marginLeft: "auto",
        marginRight: "auto",
        width: "30ch"
    },
    actions: {
        justifyContent: "flex-end"
    }
}));

export default function PokemonAdd(props) {
    const classes = useStyles();

    const [btnDisabled, setBtnDisabled] = useState(true);
    const [name, setName] = useState("");
    const [type, setType] = useState("");
    const [description, setDescription] = useState("");
    const [imgUrl, setImgUrl] = useState("");
    const [error, setError] = useState(false);

    useEffect(() => {
        validate();
    });

    /**
     * Cancel pokemon add form and retun to list
     */
    const handleCancel = () => {
        setName("");
        setType("");
        setDescription("");
        setImgUrl("");
        props.setComponent("list");
    };

    /**
     * Validate inputs to enable/disable save button
     */
    const validate = () => {
        setBtnDisabled(!(name && type && description && imgUrl));
    };

    /**
     * Use api random pokemon data to create a description
     * @param {*} data
     * @returns
     */
    const createDescription = (data) => {
        const name = data.name.charAt(0).toUpperCase() + data.name.slice(1);

        const abilities = data.abilities.map((a) => a.ability.name);
        const moves = data.moves.map((m) => m.move.name);

        const createArrayText = (arr, text) => {
            let returnText = text;
            switch (arr.length) {
                case 0:
                    returnText = "";
                    break;
                case 1:
                    returnText = `${returnText} ${arr[0]}.`;
                    break;
                case 2:
                    returnText = `${returnText} ${arr.join(" and ")}.`;
                    break;
                default:
                    const commaDel = arr.slice(0, 10).join(", ");
                    const last = arr[10];
                    returnText = `${returnText} ${commaDel}, and ${last}.`;
                    break;
            }
            return returnText;
        };

        let abilityText = createArrayText(abilities, "This pokemon has abilities such as");
        let movesText = createArrayText(moves, "It also has moves like");

        return `${name} stands at ${data.height} centimeters tall. ${abilityText} ${movesText} This pokemon is a really cool one and you should really get to know them more`;
    };

    /**
     * Get Random pokemon data to fill form
     */
    const handleRandomize = async () => {
        const rand = Math.floor(Math.random() * 898) + 1;
        let pokeBody;
        try {
            const pokeResponse = await axios.get(`https://pokeapi.co/api/v2/pokemon/${rand}`);
            pokeBody = pokeResponse.data;

            setName(pokeBody.name);
            setType(pokeBody.types[0].type.name);
            setDescription(createDescription(pokeBody));
            setImgUrl(`https://pokeres.bastionbot.org/images/pokemon/${pokeBody.id}.png`);
        } catch (error) {
            console.log(error);
        }
    };

    /**
     * POST new pokemon from inputs
     */
    const handleSave = async () => {
        try {
            const response = await axios({
                method: "post", //you can set what request you want to be: delete, get, post
                url: "https://web-app-pokemon.herokuapp.com/pokemon",
                data: { name: name, type: type, description: description, image: imgUrl }, //only include data on post/put
                headers: {
                    "User-Id": "camdenwithrow",
                    "Content-Type": "application/json"
                }
            });
            //do something on success
            const responseBody = response.data;
            setName("");
            setType("");
            setDescription("");
            setImgUrl("");
            props.setComponent("list");
        } catch (error) {
            setError(true);
        }
    };

    /**
     * Reset error
     */
    const handleDismissError = () => {
        setError(false);
    };

    return (
        <React.Fragment>
            <Card className={classes.card}>
                <CardContent>
                    <div style={{ display: "flex", justifyContent: "space-between" }}>
                        <Typography variant="h4">Add Pokemon</Typography>
                        <IconButton color="secondary" onClick={handleRandomize}>
                            <CasinoIcon />
                        </IconButton>
                    </div>
                    <TextField
                        fullWidth
                        label="Name"
                        className={classes.input}
                        value={name}
                        onChange={(e) => setName(e.target.value)}
                    />
                    <TextField
                        fullWidth
                        label="Type"
                        className={classes.input}
                        value={type}
                        onChange={(e) => setType(e.target.value)}
                    />
                    <TextField
                        fullWidth
                        label="Description"
                        multiline
                        rowsMax={5}
                        className={classes.input}
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                    <TextField
                        fullWidth
                        label="Image Url"
                        multiline
                        rowsMax={5}
                        className={classes.input}
                        value={imgUrl}
                        onChange={(e) => setImgUrl(e.target.value)}
                    />
                </CardContent>
                <CardActions className={classes.actions}>
                    <Button onClick={handleCancel}>Cancel</Button>
                    <Button onClick={handleSave} disabled={btnDisabled} color="secondary">
                        Save
                    </Button>
                </CardActions>
            </Card>
            {error && (
                <ErrorMessage message="Error occurred while attempting to add pokemon" reset={handleDismissError} />
            )}
        </React.Fragment>
    );
}

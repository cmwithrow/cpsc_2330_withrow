import React, { useState, useEffect } from "react";
import axios from "axios";
import ErrorMessage from "./ErrorMessage";
import { makeStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import Chip from "@material-ui/core/Chip";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles((theme) => ({
    card: {
        padding: theme.spacing(2)
    },
    head: {
        alignItems: "center"
    },
    title: {
        textAlign: "center",
        textTransform: "capitalize"
    },
    content: {
        alignItems: "start",
        marginTop: "50px",
        padding: "25px"
    },
    image: {
        textAlign: "center"
    },
    img: {
        maxWidth: "100%"
    },
    type: {
        display: "flex",
        alignItems: "center"
    },
    tag: {
        marginLeft: "15px",
        fontSize: "20px",
        textTransform: "capitalize"
    },
    description: {
        marginTop: "20px"
    }
}));

function BlankView(props) {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Grid container className={classes.head}>
                <Grid item xs>
                    <IconButton onClick={props.back}>
                        <ArrowBackIcon />
                    </IconButton>
                </Grid>
                <Grid item xs={8}>
                    <Skeleton />
                </Grid>
                <Grid item xs />
            </Grid>
            <Grid container className={classes.content}>
                <Grid item xs={1}>
                    <IconButton onClick={props.prev}>
                        <ArrowBackIosIcon />
                    </IconButton>
                </Grid>
                <Grid item>
                    <Skeleton height={300} width={650} />
                </Grid>
                <Grid item xs={1}>
                    <IconButton onClick={props.next}>
                        <ArrowForwardIosIcon />
                    </IconButton>
                </Grid>
            </Grid>
        </React.Fragment>
    );
}

export default function PokemonView(props) {
    const classes = useStyles();

    const [pokemon, setPokemon] = useState({});
    const [currentIndex, setCurrentIndex] = useState(props.index);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    useEffect(() => {
        console.log(props.ids);
        console.log(currentIndex);
        getPokemon();
    }, [props.ids, currentIndex]);

    /**
     * GET pokemon by id
     */
    async function getPokemon() {
        const currentId = props.ids[currentIndex];
        console.log(currentId);
        try {
            setLoading(true);
            const response = await axios.get(`https://web-app-pokemon.herokuapp.com/pokemon/${currentId}`, {
                headers: {
                    "User-Id": "camdenwithrow"
                }
            });
            setPokemon(response.data);
            setLoading(false);
        } catch (e) {
            setError(true);
        }
    }

    /**
     * Got to view previous pokemon
     */
    const handlePrev = () => {
        currentIndex === 0 ? setCurrentIndex(props.ids.length - 1) : setCurrentIndex(currentIndex - 1);
    };

    /**
     * Go to view next pokemon
     */
    const handleNext = () => {
        setCurrentIndex((currentIndex + 1) % props.ids.length);
    };

    /**
     * Return to list component
     */
    const handleBack = () => {
        props.setComponent("list");
    };

    /**
     * Reset Error
     */
    const handleDismissError = () => {
        setError(false);
    };

    return (
        <React.Fragment>
            <Card className={classes.card}>
                {loading ? (
                    <BlankView back={handleBack} next={handleNext} prev={handlePrev} />
                ) : (
                    <React.Fragment>
                        <Grid container className={classes.head}>
                            <Grid item xs>
                                <IconButton onClick={handleBack}>
                                    <ArrowBackIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={8}>
                                <Typography className={classes.title} variant="h2">
                                    {pokemon.name}
                                </Typography>
                            </Grid>
                            <Grid item xs />
                        </Grid>
                        <Grid container className={classes.content}>
                            <Grid item xs={1}>
                                <IconButton onClick={handlePrev}>
                                    <ArrowBackIosIcon />
                                </IconButton>
                            </Grid>
                            <Grid item xs={10} sm={10} md={5} className={classes.image}>
                                <img src={pokemon.image} alt={`${pokemon.name} pokemon`} className={classes.img} />
                            </Grid>
                            <Grid item xs={10} sm={10} md={5}>
                                <div className={classes.type}>
                                    <Typography variant="h5">Type:</Typography>
                                    <Chip className={classes.tag} label={pokemon.type} color="primary" />
                                </div>
                                <Typography className={classes.description}>{pokemon.description}</Typography>
                            </Grid>
                            <Grid item xs={1}>
                                <IconButton onClick={handleNext}>
                                    <ArrowForwardIosIcon />
                                </IconButton>
                            </Grid>
                        </Grid>
                    </React.Fragment>
                )}
            </Card>

            {error && (
                <ErrorMessage message="Error occurred while attempting to load pokemon" reset={handleDismissError} />
            )}
        </React.Fragment>
    );
}

import React, { useState, useEffect } from "react";
import axios from "axios";
import ErrorMessage from "./ErrorMessage";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import SearchIcon from "@material-ui/icons/Search";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import Typography from "@material-ui/core/Typography";
import { Skeleton } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650
    },
    proper: {
        textTransform: "capitalize"
    }
}));

function BlankList() {
    return (
        <React.Fragment>
            <TableRow>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
                <TableCell>
                    <Skeleton />
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

export default function List(props) {
    const uri = "https://web-app-pokemon.herokuapp.com";
    const classes = useStyles();

    const [pokemonList, setPokemonList] = useState([]);
    const [loading, setLoading] = useState(true);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [pokemonToDelete, setPokemonToDelete] = useState({});
    const [error, setError] = useState(false);
    const [errorType, setErrorType] = useState("");

    useEffect(() => {
        getPokemon();
    }, []);

    /**
     *  GET list of pokemon from api
     */
    const getPokemon = async () => {
        try {
            setLoading(true);
            const response = await axios.get(`${uri}/pokemon`, { headers: { "User-Id": "camdenwithrow" } });
            setPokemonList(response.data);
            setLoading(false);
        } catch (error) {
            setError(true);
            setErrorType("load");
        }
    };

    /**
     *  Change view to single pokemon by id
     * @param {*} id
     */
    const handleView = (id) => {
        const ids = pokemonList.map((a) => a.id);
        const idIndex = ids.indexOf(id);
        props.setComponent("view");
        props.setIndex(idIndex);
        props.setIds(ids);
    };

    /**
     * Open confirm delete modal
     * @param {*} pokemon
     */
    const handleAttemptDelete = (pokemon) => {
        setPokemonToDelete(pokemon);
        setOpenDeleteModal(true);
    };

    /**
     * Close modal to confirm delete
     */
    const handleCloseDeleteModal = () => {
        setOpenDeleteModal(false);
    };

    /**
     * DELETE pokemon with id
     * @param {*} id
     */
    const handleDelete = async () => {
        setOpenDeleteModal(false);
        try {
            const response = await axios.delete(`${uri}/pokemon/${pokemonToDelete.id}`, {
                headers: { "User-Id": "camdenwithrow" }
            });
        } catch (error) {
            setError(true);
            setErrorType("delete");
        }
        getPokemon();
        setPokemonToDelete({});
    };

    /**
     * Reset error state with dismisal of error
     */
    const handleDismissError = () => {
        setError(false);
        setErrorType("");
    };

    return (
        <React.Fragment>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Type</TableCell>
                            <TableCell align="center">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <BlankList />
                        ) : (
                            <React.Fragment>
                                {pokemonList.map((pokemon) => (
                                    <TableRow key={pokemon.id}>
                                        <TableCell className={classes.proper}>{pokemon.name}</TableCell>
                                        <TableCell className={classes.proper}>{pokemon.type}</TableCell>
                                        <TableCell align="center">
                                            <Button onClick={() => handleView(pokemon.id)}>
                                                <SearchIcon color="secondary" />
                                            </Button>
                                            <Button onClick={() => handleAttemptDelete(pokemon)}>
                                                <DeleteIcon />
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                                <TableRow>
                                    <TableCell>
                                        <Button
                                            size="small"
                                            color="primary"
                                            variant="outlined"
                                            onClick={() => props.setComponent("add")}
                                        >
                                            <AddIcon /> Add Pokemon
                                        </Button>
                                    </TableCell>
                                    <TableCell>
                                        <Skeleton width="60%" />
                                    </TableCell>
                                    <TableCell>
                                        <Skeleton />
                                    </TableCell>
                                </TableRow>
                            </React.Fragment>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            <Dialog open={openDeleteModal} onClose={handleCloseDeleteModal}>
                <DialogTitle>
                    <Typography variant="span">Are you sure you want to delete </Typography>
                    <Typography variant="span" className={classes.deletePokemon}>
                        {pokemonToDelete.name}
                    </Typography>
                    ?
                </DialogTitle>
                <DialogActions>
                    <Button onClick={handleCloseDeleteModal} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleDelete} color="primary" autoFocus>
                        Delete
                    </Button>
                </DialogActions>
            </Dialog>
            {error && (
                <ErrorMessage
                    message={`Error occurred while attempting to ${errorType} pokemon`}
                    reset={handleDismissError}
                />
            )}
        </React.Fragment>
    );
}

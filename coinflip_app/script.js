"use strict";

const heads = "images/heads.jpg";
const tails = "images/tails.jpg";

const image = document.getElementById("coin");
const result = document.getElementById("result");
const hCount = document.getElementById("heads-count");
const tCount = document.getElementById("tails-count");

let h = 0;
let t = 0;

/**
 * Flips the coin
 */
function flipCoin() {
    const rand = Math.floor(Math.random() * 2);

    rand === 1
        ? ((image.src = heads), (result.innerText = "Heads"), h++)
        : ((image.src = tails), (result.innerText = "Tails"), t++);
    hCount.innerText = h;
    tCount.innerText = t;
}
